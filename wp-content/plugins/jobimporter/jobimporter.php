<?php
/**
 * Plugin Name: Jobimporter Plugin
 * Plugin URI: http://www.mywebsite.com/my-first-plugin
 * Description: This plugin imports jobdata in the CareerFy Theme
 * Version: 1.0
 * Author: Stephanos Papadakis
 * Author URI: http://www.s-p-i-solutions.de
 */

#require 'socket/cli/server.php';


//use Jobimporter\includes;

require plugin_dir_path(__FILE__) . 'includes/Client.php';

if (!defined('ABSPATH')) {
    exit;
}



add_action('admin_menu', 'jobimporter_plugin_setup_menu');

function jobimporter_plugin_setup_menu()
{
    add_menu_page('JobImporter', 'DWF-Jobimporter', 'manage_options', 'jobimporter-plugin', 'jobimporter_init');
}

function jobimporter_init()
{

    $javascriptpath = plugin_dir_url(__FILE__) . "/js/importer.js";
    $importerPath = plugin_dir_url(__FILE__) . "import.php";






    ?>
    <script  src="<?php echo $javascriptpath; ?>"/></script>
    <script>
        setUpWebSocket();
    </script>
    <?php



    echo "<h1>DWF Jobimporter</h1><br>";

    echo '<a id="startButton" class="button button-success btn-lg btn-block" onclick="importAction();" >Start Import</a><br>';
    echo '<a id="startButton" class="button button-success btn-lg btn-block" onclick="loadtestAction();" >Load test</a><br>';
    echo "<span id='fetchdata'>Fetch Data</span>";

    ?>
        <style>
            .progress-bar {
                background-color: #1a1a1a;
                height: 30px;
                padding: 5px;
                width: 500px;
                margin: 5px 0;
                border-radius: 5px;
                box-shadow: 0 1px 5px #000 inset, 0 1px 0 #444;
            }
            .progress-bar span {
                display: inline-block;
                float: left;
                height: 100%;
                border-radius: 3px;
                box-shadow: 0 1px 0 rgba(255, 255, 255, .5) inset;
                transition: width .4s ease-in-out;
            }
            .blue span {
                background-color: #34c2e3;
            }
            .orange span {
                background-color: #fecf23;
            }
            .progress-text {
                text-align: right;
                color: white;
                margin: -25px 0px;
            }
        </style>


    <div  class="progress-bar blue">
        <span id="blueProgressBar" style="width: 0%"></span>
    </div>
    <span id='progress'>Start import</span>
    <div  class="progress-bar orange">
        <span id="orangeProgressBar" style="width: 0%"></span>
    </div>

<?php

//    echo '<a class="button button-success btn-lg btn-block" onclick="importAction();" >Whatever</a>';
//	echo '<a class="button button-success btn-lg btn-block" onclick="openImporter(\''.$importerPath.'\');" >Start Import</a>';
}



add_action('wp_ajax_import_action', 'import_action');
function import_action()
{
    global $wpdb;

    $client = new Client();
    $successFullyConnected = false;
    if($client->connect("127.0.0.1",8000,"/dwf/jobimporter")){
        $successFullyConnected = true;
        echo "SuccessFully connected To Server";
    } else {
        echo "Status Server connection failed !!";
        return;
    }


    $serviceUrl = 'http://localhost:8080/fetchAllJobs';
    $response = wp_remote_get($serviceUrl);


    //$curl = curl_init($serviceUrl);
    //curl_setopt($curl, CURLOPT_GET, true);

    //curl_setopt($curl, CURLOPT_POSTFIELDS, "newsletterId=" . $newsletterId . "&receiverEmail=" . $receiverEmail. "&userpasswordToken=" . $userpasswordToken);
    //$curl_response = curl_exec($curl);

    $decodedJSON = json_decode(wp_remote_retrieve_body($response));

    $client->sendData('{
                "action" : "initProgressBar",
                "data": "'.sizeof($decodedJSON).'"
            }');

    $elementProcentValue = 100/sizeof($decodedJSON);

    $loopCounter = 0;


    foreach ($decodedJSON as $key => $job) {
        $posttitle = $job->employer->company;

        $employerID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "' and post_status ='publish'");

        if ($employerID == null){
            $employerID = createNewEmployer($job->employer);
        }

        $my_post = array(
            'post_title' => $job->post_title,
            'post_content' => $job->meta_input->content,
            'post_status' => 'publish',
            'post_author' => 1,
            'post_type' => 'job',
            'post_category' => array(8, 39),
            'meta_input' => array(
                'jobsearch_field_job_publish_date' => $job->meta_input->jobsearch_field_job_publish_date,
                'jobsearch_field_job_expiry_date' => $job->meta_input->jobsearch_field_job_expiry_date,
                'job_actexpiry_date' => '2606491999',
                'jobsearch_field_job_application_deadline_date' => $job->meta_input->jobsearch_field_job_application_deadline_date,
                'jobsearch_field_job_apply_type' => $job->meta_input->jobsearch_field_job_apply_type,
                'jobsearch_field_job_apply_url' => $job->meta_input->jobsearch_field_job_apply_url,
                'jobsearch_field_job_apply_email' => '',
                'jobsearch_field_job_salary' => $job->meta_input->jobsearch_field_job_salary,
                'jobsearch_field_job_max_salary' => $job->meta_input->jobsearch_field_job_max_salary,
                'jobsearch_field_job_salary_type' => '',
                'jobsearch_field_job_salary_currency' => $job->meta_input->jobsearch_field_job_salary_currency,
                'jobsearch_field_job_salary_pos' => 'left',
                'jobsearch_field_job_salary_sep' => ',',
                'jobsearch_field_job_salary_deci' => '2',
                'jobsearch_field_job_featured' => '',
                'jobsearch_field_job_feature_till' => '',
                'jobsearch_field_job_filled' => 'off',
                'jobsearch_field_job_posted_by' => $employerID,
                'jobsearch_field_job_status' => 'approved',
                'jobsearch_job_presnt_status' => 'approved',
                'jobsearch_field_location_location2' => $job->meta_input->jobsearch_field_location_location2,
                'jobsearch_field_location_address' => '',
                'jobsearch_location_old_postalcode' => '',
                'jobsearch_field_location_postalcode' => '',
                'jobsearch_field_location_lat' => '',
                'jobsearch_field_location_lng' => '',
                'jobsearch_field_location_zoom' => '',
                'jobsearch_field_map_height' => '',
                'jobsearch_field_marker_image' => '',
                'jobsearch_field_package_exfield_title' => '',
                'jobsearch_field_package_exfield_val' => '',
                'jobsearch_job_single_exp_cron	yes' => '',
                'jobsearch_field_job_employer_count_updated' => 'yes',
                'jobsearch_job_employer_status' => 'approved',
                'jobsearch_job_views_count' => '',
                'jobsearch_field_urgent_job' => 'off',
                'Industry' => '',
                '_urgent_job_frmadmin' => 'yes',
                'career-level' => '',
                'careerfy_field_job_post_detail_style' => '',
                'content' => 'TestDescription',
                'cusjob_urgent_fbckend' => 'on',
                'experience' => '',
                'gender' => ''
            ),

            'tax_input' => array(
                'jobtype' => array(
                    fetchJobTypeSlug($job->tax_input[0]->jobtype[0])
                ))
        );

        $postID = wp_insert_post($my_post);

        $loopCounter++;
        $progressValue = $elementProcentValue * $loopCounter;


            $client->sendData('{
                "action" : "setProgress",
                "data": "'.$progressValue.'%",
                "filename" : "'.$my_post['post_title'].'"
            }');
    }

//     $arrayJSON = json_decode( $curl_response, true);

//     // Insert the post into the database    
//     //wp_insert_post( $my_post );
//     //echo "Success!!".$curl_response;
    echo "Success!!" . $decodedJSON;


    wp_die();
}

add_action('admin_enqueue_scripts', 'dwf_jobimporter_enqueue');
function dwf_jobimporter_enqueue($hook)
{
//     if( 'index.php' != $hook ) {
//         return;
//     }    
    wp_enqueue_script('ajax-script', plugins_url('/js/importer.js', __FILE__), array('jquery'));

    // in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
    wp_localize_script('ajax-script', 'ajax_object',
        array('ajax_url' => admin_url('admin-ajax.php'), 'we_value' => 1234));
}


function createNewEmployer($employer){
    $employer_post = array(
        'post_title' => $employer->company,
        'post_content' => '',
        'post_status' => 'publish',
        'post_author' => 1,
        'post_type' => 'employer'
    );
    $employerID = wp_insert_post($employer_post);

    $upload_file = wp_upload_bits($employer->company_logo, null, file_get_contents($employer->company_logo));

    $wp_filetype = wp_check_filetype($employer->company_logo, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_parent' => $employerID,
        'post_title' => preg_replace('/\.[^.]+$/', '', $upload_file['file']),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    //wp_insert_attachment( $attachment, $filename, $parent_post_id );
    $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $employerID );

    $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
    wp_update_attachment_metadata( $attachment_id,  $attachment_data );
    set_post_thumbnail( $employerID, $attachment_id );

    return $employerID;
}

function fetchJobTypeSlug($jobtype){


    // Get term by name ''news'' in Categories taxonomy.
    $term = get_term_by('name', $jobtype, 'jobtype');


    return  $term->term_id;
}

add_action('wp_ajax_loadtest_action', 'loadtest_action');
function loadtest_action(){
    global $wpdb;

    //echo "Start fetching<br>".date("l jS \of F Y h:i:s A",time() )."<br>";

    echo "Start fetching<br>".time()."<br>";

    $numberOfJobs = $wpdb->get_var("SELECT * FROM $wpdb->posts WHERE post_type ='job'");

    echo "Stop fetching<br>".time()."<br>";

    echo $numberOfJobs;

}
