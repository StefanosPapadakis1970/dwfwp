<?php

/**
 * Careerfy Theme Config.
 *
 * @package Careerfy
 */
define("CAREERFY_VERSION", "4.7.0");
define("WP_JOBSEARCH_VERSION", "1.5.9");

function careerfy_framework_options() {
    global $careerfy_framework_options;
    return $careerfy_framework_options;
}
