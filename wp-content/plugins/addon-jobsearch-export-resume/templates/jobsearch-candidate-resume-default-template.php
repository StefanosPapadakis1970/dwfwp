<?php


if (!class_exists('jobsearch_candidate_pdf_resume_default_template')) {

    class jobsearch_candidate_pdf_resume_default_template
    {
        public function __construct()
        {
            add_action('init', array($this, 'jobsearch_single_candidate_resume_export_callback'));
            add_action('wp_footer', array($this, 'jobsearch_single_candidate_resume_form'), 10);
            add_action('admin_footer', array($this, 'jobsearch_single_candidate_resume_form'), 10);
        }

        public function jobsearch_single_candidate_resume_form()
        {
            global $jobsearch_plugin_options, $sitepress;
            //
            $flag = false;
            $page_id = isset($jobsearch_plugin_options['user-dashboard-template-page']) ? $jobsearch_plugin_options['user-dashboard-template-page'] : '';
            $page_id = jobsearch__get_post_id($page_id, 'page');
            $lang_code = '';
            if (function_exists('icl_object_id') && function_exists('wpml_init_language_switcher')) {
                $lang_code = $sitepress->get_current_language();
            }
            if (function_exists('icl_object_id') && function_exists('wpml_init_language_switcher')) {
                $page_id = icl_object_id($page_id, 'page', false, $lang_code);
            }
            if (is_page($page_id)) {
                $flag = true;
            }
            if (is_admin()) {
                $flag = true;
            }

            if ($flag == false) {
                return;
            }
            ?>
            <form id="pdf_cand_generate_form" method="post" enctype="multipart/form-data" style="display: none">
                <input type="text" name="jobsearch_single_pdf_cand_id" value="">
                <input type="submit" class="btn btn-default" name="pdf_cand_generate_form_submit"
                       value="Generate PDF">
            </form>
            <script type="text/javascript">
                jQuery(".jobsearch-get-cand-id").on('click', function () {
                    var _cand_id = jQuery(this).attr('data-cand-id');
                    jQuery("input[name=jobsearch_single_pdf_cand_id]").val(_cand_id);
                    setTimeout(function () {
                        jQuery("input[name=pdf_cand_generate_form_submit]").trigger('click')
                    }, 300)
                })

            </script>
        <?php }

        public function jobsearch_single_candidate_resume_export_callback()
        {
            global $jobsearch_resume_export;

            if (isset($_POST['pdf_cand_generate_form_submit'])) {

                $candidate_id = $_POST['jobsearch_single_pdf_cand_id'];
                $stylesheet = file_get_contents($jobsearch_resume_export->jobsearch_resume_export_get_path('css/jobsearch-mpdf-style.css'));
                $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
                $fontDirs = $defaultConfig['fontDir'];

                $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
                $fontData = $defaultFontConfig['fontdata'];
                $mpdf = new \Mpdf\Mpdf([
                    'mode' => 'utf-8',
                    'format' => 'A4',
                    'margin_left' => 5,
                    'margin_right' => 5,
                    'margin_top' => 13,
                    'margin_bottom' => 13,
                    'border' => '2px solid #000',
                    'mirrorMargins' => true,
                    'tempDir' => __DIR__ . '/upload',
                    'fontDir' => array_merge($fontDirs, [
                        __DIR__ . '/fonts'
                    ]),
                    'fontdata' => $fontData + [
                            "proximanova" => [
                                'R' => "ProximaNova-Regular.ttf",
                            ],
                            "jobsearch" => [
                                'R' => "icomoon.ttf",
                            ],
                        ],
                    'default_font' => 'proximanova'
                ]);

                $user_id = jobsearch_get_candidate_user_id($candidate_id);
                $user_obj = get_user_by('ID', $user_id);
                $user_displayname = isset($user_obj->display_name) ? $user_obj->display_name : '';
                $user_displayname = apply_filters('jobsearch_user_display_name', $user_displayname, $user_obj);
                $candidate_obj = get_post($candidate_id);
                $candidate_content = $candidate_obj->post_content;
                $candidate_content = apply_filters('the_content', $candidate_content);

                $user_website = isset($user_obj->user_url) ? $user_obj->user_url : '';
                $user_email = isset($user_obj->user_email) ? $user_obj->user_email : '';
                //
                $jobsearch_candidate_jobtitle = get_post_meta($candidate_id, 'jobsearch_field_candidate_jobtitle', true);
                $candidate_company_str = '';
                if ($jobsearch_candidate_jobtitle != '') {
                    $candidate_company_str .= $jobsearch_candidate_jobtitle;
                }
                // Extra Fields
                $user_def_avatar_url = jobsearch_candidate_img_url_comn($candidate_id);
                $profile_image = $user_def_avatar_url;
                $user_id = jobsearch_get_candidate_user_id($candidate_id);
                $user_obj = get_user_by('ID', $user_id);
                $cand_email = $user_obj->user_email;
                $user_firstname = isset($user_obj->first_name) ? $user_obj->first_name : '';
                $user_displayname = isset($user_obj->display_name) ? $user_obj->display_name : '';
                //
                $phone_number = get_post_meta($candidate_id, 'jobsearch_field_user_phone', true);
                ob_start();

                ?>
                <div class="cndt-body">
                    <div class="cndt-user-section">
                        <?php if (!empty($profile_image)) { ?>
                            <div class="cndt-user-thumb"><img src="<?php echo($profile_image) ?>" alt=""></div>
                        <?php } ?>
                        <div class="cndt-user-text">
                            <div class="cndt-name"><?php echo($user_displayname) ?></div>
                            <div class="cndt-jobtitle"><?php echo jobsearch_esc_html($candidate_company_str) ?></div>
                            <div class="cndt-contact-info">
                                <span><?php echo esc_html__('PHONE:', 'jobsearch-resume-export') ?></span>
                                <a href="tel:<?php echo($phone_number) ?>"><?php echo($phone_number) ?></a>
                                <span><?php echo esc_html__('EMAIL:', 'jobsearch-resume-export') ?></span>
                                <a href="mailto:<?php echo($cand_email) ?>"><?php echo($cand_email) ?></a>
                            </div>
                        </div>
                    </div>

                    <div class="cndt-left-content">
                        <!--Candidate Custom Fields-->
                        <?php echo self::jobsearch_resume_candidate_custom_fields($candidate_id) ?>
                        <!--Candidate Experience-->
                        <?php echo self::jobsearch_resume_candidate_experience($candidate_id) ?>
                        <!--Candidate Education-->
                        <?php echo self::jobsearch_resume_candidate_education($candidate_id) ?>
                        <!--Candidate Awards-->
                        <?php echo self::jobsearch_resume_candidate_awards($candidate_id) ?>
                        <!--Portfolio-->
                        <?php echo self::jobsearch_resume_cand_portfolio($candidate_id) ?>
                    </div>

                    <div class="cndt-right-content">
                        <div class="content-icon-wrap">
                            <div class="content-icon">
                                <div style="font-family: jobsearch">&#xe943</div>
                            </div>
                            <div class="cndt-content-title">
                                <span><?php echo esc_html__('About Me', 'jobsearch-resume-export') ?></span>
                            </div>
                        </div>

                        <div class="cndt-right-pera"><?php echo($candidate_content) ?></div>
                        <!--Candidate Expertise-->
                        <?php echo self::jobsearch_resume_candidate_expertise($candidate_id) ?>
                        <div class="sidebar-spacer"></div>
                        <!--Candidate Languages-->
                        <?php echo self::jobsearch_resume_candidate_languages($candidate_id) ?>
                        <div class="sidebar-spacer"></div>
                        <!--Candidate Skills-->
                        <?php echo self::jobsearch_resume_candidate_skills($candidate_id) ?>
                    </div>
                </div>

                <?php
                $pdf_html = ob_get_clean();
                $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
                $mpdf->WriteHTML($pdf_html, \Mpdf\HTMLParserMode::HTML_BODY);
                $mpdf->Output($user_firstname . '-' . date('dmy') . "-" . $candidate_id . '.pdf', 'D');
            }
        }

        public function jobsearch_candidate_resume_bulk_export_template_default($candidate_id, $mpdf)
        {
            global $jobsearch_resume_export, $jobsearch_pdf_temp_upload_file;
            $stylesheet = file_get_contents($jobsearch_resume_export->jobsearch_resume_export_get_path('css/jobsearch-mpdf-style.css'));

            $user_id = jobsearch_get_candidate_user_id($candidate_id);
            $user_obj = get_user_by('ID', $user_id);

            $user_displayname = isset($user_obj->display_name) ? $user_obj->display_name : '';
            $user_displayname = apply_filters('jobsearch_user_display_name', $user_displayname, $user_obj);
            $candidate_obj = get_post($candidate_id);
            $candidate_content = $candidate_obj->post_content;
            $candidate_content = apply_filters('the_content', $candidate_content);

            $user_website = isset($user_obj->user_url) ? $user_obj->user_url : '';
            $cand_email = isset($user_obj->user_email) ? $user_obj->user_email : '';
            //
            $jobsearch_candidate_jobtitle = get_post_meta($candidate_id, 'jobsearch_field_candidate_jobtitle', true);
            $candidate_company_str = '';
            if ($jobsearch_candidate_jobtitle != '') {
                $candidate_company_str .= $jobsearch_candidate_jobtitle;
            }

            // Extra Fields
            $user_def_avatar_url = jobsearch_candidate_img_url_comn($candidate_id);
            $profile_image = $user_def_avatar_url;
            $user_firstname = isset($user_obj->first_name) ? $user_obj->first_name : '';
            $user_displayname = isset($user_obj->display_name) ? $user_obj->display_name : '';
            //
            $phone_number = get_post_meta($candidate_id, 'jobsearch_field_user_phone', true);
            ob_start();

            ?>
            <div class="cndt-body">
                <div class="cndt-user-section">
                    <?php if (!empty($profile_image)) { ?>
                        <div class="cndt-user-thumb"><img src="<?php echo($profile_image) ?>" alt=""></div>
                    <?php } ?>
                    <div class="cndt-user-text">
                        <div class="cndt-name"><?php echo($user_displayname) ?></div>
                        <div class="cndt-jobtitle"><?php echo jobsearch_esc_html($candidate_company_str) ?></div>
                        <div class="cndt-contact-info">
                            <span><?php echo esc_html__('PHONE:', 'jobsearch-resume-export') ?></span>
                            <a href="tel:<?php echo($phone_number) ?>"><?php echo($phone_number) ?></a>
                            <span><?php echo esc_html__('EMAIL:', 'jobsearch-resume-export') ?></span>
                            <a href="mailto:<?php echo($cand_email) ?>"><?php echo($cand_email) ?></a>
                        </div>
                    </div>
                </div>

                <div class="cndt-left-content">
                    <!--Candidate Custom Fields-->
                    <?php echo self::jobsearch_resume_candidate_custom_fields($candidate_id) ?>
                    <!--Candidate Experience-->
                    <?php echo self::jobsearch_resume_candidate_experience($candidate_id) ?>
                    <!--Candidate Education-->
                    <?php echo self::jobsearch_resume_candidate_education($candidate_id) ?>
                    <!--Candidate Awards-->
                    <?php echo self::jobsearch_resume_candidate_awards($candidate_id) ?>
                    <!--Portfolio-->
                    <?php echo self::jobsearch_resume_cand_portfolio($candidate_id) ?>
                </div>

                <div class="cndt-right-content">
                    <div class="content-icon-wrap">
                        <div class="content-icon">
                            <div style="font-family: jobsearch">&#xe943</div>
                        </div>
                        <div class="cndt-content-title">
                            <span><?php echo esc_html__('About Me', 'jobsearch-resume-export') ?></span>
                        </div>
                    </div>
                    <div class="cndt-right-pera"><?php echo($candidate_content) ?></div>
                    <!--Candidate Expertise-->
                    <?php echo self::jobsearch_resume_candidate_expertise($candidate_id) ?>
                    <div class="sidebar-spacer"></div>
                    <!--Candidate Languages-->
                    <?php echo self::jobsearch_resume_candidate_languages($candidate_id) ?>
                    <div class="sidebar-spacer"></div>
                    <!--Candidate Skills-->
                    <?php echo self::jobsearch_resume_candidate_skills($candidate_id) ?>
                </div>
            </div>

            <?php
            if (file_exists(JOBSEARCH_RESUME_PDF_TEMP_DIR_PATH)) {
                $location = JOBSEARCH_RESUME_PDF_TEMP_DIR_PATH;
            } else {
                $jobsearch_pdf_temp_upload_file = true;
                add_filter('upload_dir', 'jobsearch_resume_export_files_upload_dir', 10, 1);
                $wp_upload_dir = wp_upload_dir();
                $location = $wp_upload_dir['path'] . "/";
                remove_filter('upload_dir', 'jobsearch_resume_export_files_upload_dir', 10, 1);
                $jobsearch_pdf_temp_upload_file = false;
            }

            $pdf_html = ob_get_clean();
            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML($pdf_html, \Mpdf\HTMLParserMode::HTML_BODY);
            $mpdf->Output($location . $user_firstname . '-' . date('dmy') . "-" . $candidate_id . '.pdf', 'F');
        }

        public static function jobsearch_resume_candidate_custom_fields($candidate_id)
        {
            $custom_all_fields = get_option('jobsearch_custom_field_candidate');
            if (!empty($custom_all_fields)) { ?>
                <div class="content-icon-wrap">
                    <div class="content-icon">
                        <div>&#xe940</div>
                    </div>
                    <div class="cndt-content-title">
                        <span><?php echo esc_html__('About Me', 'jobsearch-resume-export') ?></span>
                    </div>
                </div>
                <div class="cndt-custom-field">
                    <?php
                    foreach ($custom_all_fields as $info) {
                        if (!empty($info['name'])) {
                            $field_value = get_post_meta($candidate_id, $info['name'], true);
                            ?>
                            <div class="cndt-custom-field-inner">
                                <?php if (!empty($info['icon'])) { ?>
                                    <div class="cndt-custom-field-icon-wrap">
                                        <div class="cndt-custom-field-icon">
                                            <div><?php echo jobsearch_get_font_code($info['icon']) ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="cndt-custom-field-text">
                                    <div class="cndt-custom-field-title"><?php echo($info['label']) ?></div>
                                    <?php if (is_array($field_value) && count($field_value) > 0) {
                                        foreach ($field_value as $val) { ?>
                                            <div class="cndt-custom-field-sub"><?php echo($val) ?></div>
                                        <?php }
                                    } else { ?>
                                        <div class="cndt-custom-field-sub"><?php echo($field_value) ?></div>
                                    <?php } ?>

                                </div>
                            </div>
                        <?php }
                    } ?>
                </div>
            <?php }
        }

        public function jobsearch_resume_candidate_experience($candidate_id)
        {
            $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_experience_title', true);
            $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_experience_description', true);
            $experience_start_datefield_list = get_post_meta($candidate_id, 'jobsearch_field_experience_start_date', true);
            $experience_end_datefield_list = get_post_meta($candidate_id, 'jobsearch_field_experience_end_date', true);
            $experience_prsnt_datefield_list = get_post_meta($candidate_id, 'jobsearch_field_experience_date_prsnt', true);
            $experience_company_field_list = get_post_meta($candidate_id, 'jobsearch_field_experience_company', true);
            if (is_array($exfield_list) && sizeof($exfield_list) > 0) { ?>

                <div class="content-icon-wrap">
                    <div class="content-icon">
                        <div style="font-family: jobsearch">&#xe940</div>
                    </div>
                    <div class="cndt-content-title">
                        <span><?php echo esc_html__('Work Experience', 'jobsearch-resume-export'); ?></span>
                    </div>
                </div>
                <?php
                $exfield_counter = 0;
                foreach ($exfield_list as $exfield) {
                    $exfield_val = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                    $experience_start_datefield_val = isset($experience_start_datefield_list[$exfield_counter]) ? $experience_start_datefield_list[$exfield_counter] : '';
                    $experience_end_datefield_val = isset($experience_end_datefield_list[$exfield_counter]) ? $experience_end_datefield_list[$exfield_counter] : '';
                    $experience_prsnt_datefield_val = isset($experience_prsnt_datefield_list[$exfield_counter]) ? $experience_prsnt_datefield_list[$exfield_counter] : '';
                    $experience_end_companyfield_val = isset($experience_company_field_list[$exfield_counter]) ? $experience_company_field_list[$exfield_counter] : '';
                    ?>

                    <div class="cndt-contant-article">
                        <span class="cndt-contant-article-sub"><?php echo($experience_end_companyfield_val) ?></span>
                        <div class="cndt-contant-article-min"><?php echo jobsearch_esc_html($exfield) ?></div>
                        <?php if ($experience_prsnt_datefield_val == 'on') { ?>
                            <div class="cndt-contant-article-date"><?php echo ($experience_start_datefield_val != '' ? date('Y', strtotime($experience_start_datefield_val)) : '') . (' - ') . esc_html__('Present', 'jobsearch-resume-export') ?></div>
                        <?php } else { ?>
                            <div class="cndt-contant-article-date"><?php echo ($experience_start_datefield_val != '' ? date('Y', strtotime($experience_start_datefield_val)) : '') . ($experience_end_datefield_val != '' ? ' - ' . date('Y', strtotime($experience_end_datefield_val)) : '') ?></div>
                        <?php } ?>
                        <div class="cndt-contant-article-pera">
                            <?php echo jobsearch_esc_html($exfield_val) ?>
                        </div>
                    </div>
                    <?php $exfield_counter++;
                } ?>
            <?php }
        }

        public static function jobsearch_resume_candidate_education($candidate_id)
        {
            $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_education_title', true);
            $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_education_description', true);
            $education_academyfield_list = get_post_meta($candidate_id, 'jobsearch_field_education_academy', true);
            $education_yearfield_list = get_post_meta($candidate_id, 'jobsearch_field_education_year', true);
            if (is_array($exfield_list) && sizeof($exfield_list) > 0) { ?>
                <div class="content-icon-wrap">
                    <div class="content-icon">
                        <div style="font-family: jobsearch ;">&#xe944</div>
                    </div>
                    <div class="cndt-content-title">
                        <span><?php echo esc_html__('EDUCATION', 'jobsearch-resume-export') ?></span>
                    </div>
                </div>
                <?php
                $exfield_counter = 0;
                foreach ($exfield_list as $exfield) {
                    $exfield_val = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                    $education_academyfield_val = isset($education_academyfield_list[$exfield_counter]) ? $education_academyfield_list[$exfield_counter] : '';
                    $education_yearfield_val = isset($education_yearfield_list[$exfield_counter]) ? $education_yearfield_list[$exfield_counter] : ''; ?>
                    <div class="cndt-contant-article">
                        <span class="cndt-contant-article-sub"><?php echo($exfield) ?></span>
                        <div class="cndt-contant-article-min"><?php echo($exfield_val) ?></div>
                        <div class="cndt-contant-article-date"><?php echo($education_yearfield_val) ?></div>
                    </div>
                    <?php $exfield_counter++;
                } ?>
            <?php }
        }

        public static function jobsearch_resume_candidate_awards($candidate_id)
        {
            $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_award_title', true);
            $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_award_description', true);
            $award_yearfield_list = get_post_meta($candidate_id, 'jobsearch_field_award_year', true);
            if (is_array($exfield_list) && sizeof($exfield_list) > 0) { ?>
                <div class="content-icon-wrap">
                    <div class="content-icon">
                        <div style="font-family: jobsearch">&#xe940</div>
                    </div>
                    <div class="cndt-content-title">
                        <span><?php echo esc_html__('HONORS & AWARDS', 'jobsearch-resume-export'); ?></span>
                    </div>
                </div>
                <?php
                $rand_num = rand(1000000, 99999999);
                $exfield_counter = 0;
                foreach ($exfield_list as $exfield) {
                    $exfield_val = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                    $award_yearfield_val = isset($award_yearfield_list[$exfield_counter]) ? $award_yearfield_list[$exfield_counter] : '';
                    ?>
                    <div class="cndt-contant-article">
                        <span class="cndt-contant-article-sub"><?php echo jobsearch_esc_html($exfield) ?></span>
                        <div class="cndt-contant-article-min"><?php echo jobsearch_esc_html($exfield_val) ?></div>
                        <div class="cndt-contant-article-date"><?php echo jobsearch_esc_html($award_yearfield_val) ?></div>
                    </div>
                    <?php $exfield_counter++;
                }
            }
        }

        public static function jobsearch_resume_candidate_skills($candidate_id)
        {
            $skills_list = jobsearch_resume_export_job_get_all_skills($candidate_id, '', '', '', '', '<div class="cndt-skills-inner"><div class="cndt-skills-list-item">', '</div></div>', 'candidate');
            $skills_list = apply_filters('jobsearch_cand_detail_skills_list_html', $skills_list, $candidate_id);
            if (!empty($skills_list)) { ?>
                <div class="content-icon-wrap">
                    <div class="content-icon">
                        <div style="font-family: jobsearch">&#xe93f</div>
                    </div>
                    <div class="cndt-content-title">
                        <span><?php echo esc_html__('Skills', 'jobsearch-resume-export') ?></span>
                    </div>
                </div>
                <div class="cndt-skills">
                    <?php if ($skills_list != '') { ?>
                        <?php echo($skills_list); ?>
                    <?php } ?>
                </div>
            <?php }
        }

        public static function jobsearch_resume_candidate_languages($candidate_id)
        {
            $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_lang_title', true);
            $lang_percentagefield_list = get_post_meta($candidate_id, 'jobsearch_field_lang_percentage', true);
            $lang_level_list = get_post_meta($candidate_id, 'jobsearch_field_lang_level', true);

            if (is_array($exfield_list) && sizeof($exfield_list) > 0) { ?>
                <div class="content-icon-wrap">
                    <div class="content-icon">
                        <div style="font-family: jobsearch">&#xe93f</div>
                    </div>
                    <div class="cndt-content-title">
                        <span><?php echo esc_html__('Languages', 'jobsearch-resume-export') ?></span>
                    </div>
                </div>
                <?php
                $exfield_counter = 0;
                foreach ($exfield_list as $exfield) {
                    $rand_num = rand(1000000, 99999999);
                    $lang_percentagefield_val = isset($lang_percentagefield_list[$exfield_counter]) ? absint($lang_percentagefield_list[$exfield_counter]) : '';
                    $lang_percentagefield_val = $lang_percentagefield_val > 100 ? 100 : $lang_percentagefield_val;
                    $lang_level_val = isset($lang_level_list[$exfield_counter]) ? ($lang_level_list[$exfield_counter]) : '';

                    $lang_level_str = esc_html__('Beginner', 'wp-jobsearch');
                    if ($lang_level_val == 'proficient') {
                        $lang_level_str = esc_html__('Proficient', 'wp-jobsearch');
                    } else if ($lang_level_val == 'intermediate') {
                        $lang_level_str = esc_html__('Intermediate', 'wp-jobsearch');
                    }
                    ?>
                    <div class="cndt-right-links-wrap">
                        <div class="cndt-right-links">
                            <strong><?php echo($exfield) ?></strong> <?php echo($lang_level_str) ?>
                        </div>
                        <div class="cndt-expertise-lines">
                            <div class="cndt-expertise-lines-inn"
                                 style="width: <?php echo($lang_percentagefield_val) ?>%;"></div>
                        </div>
                    </div>
                    <?php
                    $exfield_counter++;
                }

            }
        }

        public static function jobsearch_resume_candidate_expertise($candidate_id)
        {
            $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_skill_title', true);
            $skill_percentagefield_list = get_post_meta($candidate_id, 'jobsearch_field_skill_percentage', true);
            if (is_array($exfield_list) && sizeof($exfield_list) > 0) { ?>
                <div class="content-icon-wrap">
                    <div class="content-icon">
                        <div style="font-family: jobsearch">&#xe93f</div>
                    </div>
                    <div class="cndt-content-title">
                        <span><?php echo esc_html__('EXPERTISE', 'jobsearch-resume-export') ?></span>
                    </div>
                </div>
                <?php
                $exfield_counter = 0;
                foreach ($exfield_list as $exfield) {
                    $rand_num = rand(1000000, 99999999);
                    $skill_percentagefield_val = isset($skill_percentagefield_list[$exfield_counter]) ? absint($skill_percentagefield_list[$exfield_counter]) : '';
                    $skill_percentagefield_val = $skill_percentagefield_val > 100 ? 100 : $skill_percentagefield_val;
                    ?>
                    <div class="cndt-right-links-wrap">
                        <div class="cndt-right-links">
                            <?php echo($exfield) ?> <?php echo($skill_percentagefield_val) ?>%
                        </div>
                        <div class="cndt-expertise-lines">
                            <div class="cndt-expertise-lines-inn"
                                 style="width: <?php echo($skill_percentagefield_val) ?>%;"></div>
                        </div>
                    </div>
                    <?php $exfield_counter++;
                } ?>
            <?php }
        }

        public static function jobsearch_resume_cand_portfolio($candidate_id)
        {
            $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_portfolio_title', true);
            $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_portfolio_image', true);
            $exfield_portfolio_url = get_post_meta($candidate_id, 'jobsearch_field_portfolio_url', true);
            $exfield_portfolio_vurl = get_post_meta($candidate_id, 'jobsearch_field_portfolio_vurl', true);

            if (is_array($exfield_list) && sizeof($exfield_list) > 0) { ?>
                <div class="jobsearch-pdf-main-list">
                    <h2><?php echo esc_html__('Portfolio', 'jobsearch-resume-export') ?></h2>
                    <?php
                    $exfield_counter = 0;
                    foreach ($exfield_list as $exfield) {
                        $portfolio_img = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                        $portfolio_url = isset($exfield_portfolio_url[$exfield_counter]) ? $exfield_portfolio_url[$exfield_counter] : '';
                        $portfolio_vurl = isset($exfield_portfolio_vurl[$exfield_counter]) ? $exfield_portfolio_vurl[$exfield_counter] : '';
                        $file_path = jobsearch_get_cand_portimg_path($candidate_id, $portfolio_img);
                        ?>
                        <div class="jobsearch-pdf-porfolio-img">
                            <?php if (!empty($file_path)) { ?>
                                <img src="<?php echo($file_path) ?>">
                            <?php } ?>
                            <br>
                            <div class="jobsearch-pdf-porfolio-link">
                                <?php if (!empty($exfield)) {
                                    echo $exfield . "<br>";
                                } ?>
                                <?php if (!empty($portfolio_url)) { ?>
                                    <?php echo esc_html__('Portfolio URL: ', 'jobsearch-resume-export'); ?><br>
                                    <a
                                            href="<?php echo($portfolio_url) ?>"><?php echo($portfolio_url); ?></a><br>
                                <?php } ?>
                                <?php if (!empty($portfolio_vurl)) { ?>
                                    <?php echo esc_html__('Video URL: ', 'jobsearch-resume-export'); ?><br>
                                    <a href="<?php echo($portfolio_vurl) ?>"><?php echo($portfolio_vurl); ?></a><br>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        $exfield_counter++;
                    }
                    ?>
                </div>
            <?php }
        }
    }
}
global $jobsearch_resume_pdf_default_template;
$jobsearch_resume_pdf_default_template = new jobsearch_candidate_pdf_resume_default_template();