#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Addon Jobsearch Resume Export\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-07 13:33+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.4.3; wp-5.5.1"

#: addon-jobsearch-export-resume.php:142
msgid ""
"<em><b>Addon Jobsearch Resume Export</b></em> needs the <b>WP Jobsearch</b> "
"plugin. Please install and activate it."
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:235
#: includes/jobsearch-resume-export-admin-hooks.php:771
#: templates/jobsearch-candidate-resume-default-template.php:159
#: templates/jobsearch-candidate-resume-default-template.php:252
#: templates/jobsearch-candidate-resume-default-template.php:294
#: templates/jobsearch-candidate-resume-template-one.php:336
#: templates/jobsearch-candidate-resume-template-four.php:159
#: templates/jobsearch-candidate-resume-template-four.php:289
msgid "About Me"
msgstr ""

#: templates/jobsearch-candidate-resume-template-four.php:370
#: templates/jobsearch-candidate-resume-template-five.php:491
#: templates/jobsearch-candidate-resume-template-two.php:179
#: templates/jobsearch-candidate-resume-template-two.php:364
msgid "About me"
msgstr ""

#: addon-jobsearch-export-resume.php:107 addon-jobsearch-export-resume.php:120
#: includes/jobsearch-resume-export-hooks.php:91
#: includes/jobsearch-resume-export-hooks.php:157
msgid "Active"
msgstr ""

#: includes/common-functions.php:100
msgid "Add Resume Introduction"
msgstr ""

#. Name of the plugin
msgid "Addon Jobsearch Resume Export"
msgstr ""

#: templates/jobsearch-candidate-resume-template-four.php:172
#: templates/jobsearch-candidate-resume-template-four.php:302
msgid "Address:"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:175
#: templates/jobsearch-candidate-resume-template-three.php:301
#, php-format
msgid "Address: %s"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:149
#: templates/jobsearch-candidate-resume-template-three.php:275
msgid "Biography"
msgstr ""

#: templates/jobsearch-candidate-resume-template-one.php:181
#: templates/jobsearch-candidate-resume-template-one.php:299
msgid "biography"
msgstr ""

#: includes/common-functions.php:72
msgid ""
"Candidate can turn on/off his resume box in Candidate dashboard => My Resume."
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:353
#: includes/jobsearch-resume-export-admin-hooks.php:475
#: includes/common-functions.php:466
msgid "Candidate PDF Resume"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:452
msgid "Click image to select Template"
msgstr ""

#: templates/jobsearch-candidate-resume-template-one.php:146
#: templates/jobsearch-candidate-resume-template-one.php:266
msgid "Contact"
msgstr ""

#: templates/jobsearch-candidate-resume-template-four.php:166
#: templates/jobsearch-candidate-resume-template-four.php:296
msgid "DOB:"
msgstr ""

#: includes/jobsearch-resume-export-hooks.php:104
#: includes/jobsearch-resume-export-hooks.php:177
msgid "Download PDF"
msgstr ""

#: templates/jobsearch-candidate-resume-template-five.php:205
#: templates/jobsearch-candidate-resume-template-five.php:394
msgid "Dribble"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:178
#: includes/jobsearch-resume-export-admin-hooks.php:922
#: templates/jobsearch-candidate-resume-template-three.php:453
#: templates/jobsearch-candidate-resume-default-template.php:386
#: templates/jobsearch-candidate-resume-template-one.php:462
#: templates/jobsearch-candidate-resume-template-two.php:556
msgid "EDUCATION"
msgstr ""

#: templates/jobsearch-candidate-resume-template-four.php:460
#: templates/jobsearch-candidate-resume-template-five.php:578
msgid "Education"
msgstr ""

#: templates/jobsearch-candidate-resume-template-two.php:158
#: templates/jobsearch-candidate-resume-template-two.php:343
msgid "Email. "
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:115
#: templates/jobsearch-candidate-resume-default-template.php:134
#: templates/jobsearch-candidate-resume-default-template.php:227
msgid "EMAIL:"
msgstr ""

#: templates/jobsearch-candidate-resume-template-four.php:184
#: templates/jobsearch-candidate-resume-template-four.php:314
msgid "Email:"
msgstr ""

#: templates/jobsearch-candidate-resume-template-two.php:513
msgid "Experience"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:250
#: templates/jobsearch-candidate-resume-template-three.php:546
#: templates/jobsearch-candidate-resume-default-template.php:513
#: templates/jobsearch-candidate-resume-template-one.php:538
#: templates/jobsearch-candidate-resume-template-four.php:543
#: templates/jobsearch-candidate-resume-template-five.php:654
#: templates/jobsearch-candidate-resume-template-two.php:629
msgid "EXPERTISE"
msgstr ""

#: addon-jobsearch-export-resume.php:117
#: includes/jobsearch-resume-export-hooks.php:464
msgid "Export"
msgstr ""

#: includes/jobsearch-resume-export-hooks.php:478
msgid "Export To Excel"
msgstr ""

#: addon-jobsearch-export-resume.php:119
#: includes/jobsearch-resume-export-admin-hooks.php:516
msgid "Export to Excel"
msgstr ""

#: includes/jobsearch-resume-export-hooks.php:472
msgid "Export To PDF"
msgstr ""

#: addon-jobsearch-export-resume.php:118
#: includes/jobsearch-resume-export-admin-hooks.php:522
msgid "Export to PDF"
msgstr ""

#: includes/user-uploaded-resume-export.php:80
#: includes/user-uploaded-resume-export.php:138
msgid "Export Uploaded Resumes"
msgstr ""

#. Author of the plugin
msgid "Eyecix"
msgstr ""

#: templates/jobsearch-candidate-resume-template-five.php:181
#: templates/jobsearch-candidate-resume-template-five.php:370
msgid "Facebook"
msgstr ""

#: templates/jobsearch-candidate-resume-template-five.php:178
#: templates/jobsearch-candidate-resume-template-five.php:367
msgid "follow me"
msgstr ""

#: includes/jobsearch-resume-export-hooks.php:27
msgid "Generate PDF"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:207
#: includes/jobsearch-resume-export-admin-hooks.php:953
#: templates/jobsearch-candidate-resume-default-template.php:416
msgid "HONORS & AWARDS"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:508
#: templates/jobsearch-candidate-resume-template-one.php:508
#: templates/jobsearch-candidate-resume-template-four.php:507
#: templates/jobsearch-candidate-resume-template-five.php:626
#: templates/jobsearch-candidate-resume-template-two.php:600
msgid "Honors & Awards"
msgstr ""

#. URI of the plugin
#. Author URI of the plugin
msgid "https://themeforest.net/user/eyecix/"
msgstr ""

#: templates/jobsearch-candidate-resume-template-five.php:197
#: templates/jobsearch-candidate-resume-template-five.php:386
msgid "Instagram"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:398
#: templates/jobsearch-candidate-resume-template-four.php:410
#: templates/jobsearch-candidate-resume-template-five.php:535
msgid "Job Experiences"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:632
#: templates/jobsearch-candidate-resume-default-template.php:469
#: templates/jobsearch-candidate-resume-template-one.php:567
#: templates/jobsearch-candidate-resume-template-four.php:582
#: templates/jobsearch-candidate-resume-template-five.php:689
#: templates/jobsearch-candidate-resume-template-two.php:708
msgid "Languages"
msgstr ""

#: includes/common-functions.php:84
msgid "My Resume Box"
msgstr ""

#: includes/common-functions.php:71
msgid "My Resume Box on/off"
msgstr ""

#: includes/common-functions.php:76
msgid "Off"
msgstr ""

#: includes/common-functions.php:75
msgid "On"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:452
msgid "Package Already used"
msgstr ""

#: includes/common-functions.php:595
msgid "Package Subscribed Successfully."
msgstr ""

#: templates/jobsearch-candidate-resume-template-two.php:155
#: templates/jobsearch-candidate-resume-template-two.php:340
msgid "ph. +"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:113
#: templates/jobsearch-candidate-resume-default-template.php:132
#: templates/jobsearch-candidate-resume-default-template.php:225
msgid "PHONE:"
msgstr ""

#: templates/jobsearch-candidate-resume-template-four.php:178
#: templates/jobsearch-candidate-resume-template-four.php:308
msgid "Phone:"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:581
#: templates/jobsearch-candidate-resume-default-template.php:546
#: templates/jobsearch-candidate-resume-template-one.php:412
#: templates/jobsearch-candidate-resume-template-four.php:634
#: templates/jobsearch-candidate-resume-template-five.php:737
#: templates/jobsearch-candidate-resume-template-two.php:660
msgid "Portfolio"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:603
#: templates/jobsearch-candidate-resume-default-template.php:565
#: templates/jobsearch-candidate-resume-template-one.php:432
#: templates/jobsearch-candidate-resume-template-four.php:658
#: templates/jobsearch-candidate-resume-template-five.php:758
#: templates/jobsearch-candidate-resume-template-two.php:682
msgid "Portfolio URL: "
msgstr ""

#: includes/jobsearch-resume-export-hooks.php:423
#: includes/jobsearch-resume-export-admin-hooks.php:154
#: includes/jobsearch-resume-export-admin-hooks.php:685
#: includes/jobsearch-resume-export-admin-hooks.php:896
#: templates/jobsearch-candidate-resume-template-three.php:419
#: templates/jobsearch-candidate-resume-default-template.php:361
#: templates/jobsearch-candidate-resume-template-one.php:387
#: templates/jobsearch-candidate-resume-template-four.php:430
#: templates/jobsearch-candidate-resume-template-five.php:550
#: templates/jobsearch-candidate-resume-template-two.php:526
msgid "Present"
msgstr ""

#: includes/jobsearch-resume-export-hooks.php:181
msgid "Price: "
msgstr ""

#: templates/jobsearch-candidate-resume-template-five.php:158
#: templates/jobsearch-candidate-resume-template-five.php:347
msgid "Profile"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:332
msgid "Profile PDF"
msgstr ""

#: includes/common-functions.php:591
msgid "redirecting..."
msgstr ""

#: includes/common-functions.php:98
msgid "Resume Export Settings"
msgstr ""

#: includes/jobsearch-resume-export-hooks.php:231
#: includes/jobsearch-resume-export-admin-hooks.php:487
msgid "Select All"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:408
msgid "Select PDF Templates"
msgstr ""

#: includes/common-functions.php:583
msgid "Selected Package Product not found."
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:342
msgid "Show in PDF"
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:284
#: includes/jobsearch-resume-export-admin-hooks.php:984
#: templates/jobsearch-candidate-resume-template-three.php:675
#: templates/jobsearch-candidate-resume-default-template.php:446
#: templates/jobsearch-candidate-resume-template-one.php:601
#: templates/jobsearch-candidate-resume-template-four.php:681
#: templates/jobsearch-candidate-resume-template-five.php:782
#: templates/jobsearch-candidate-resume-template-two.php:746
msgid "Skills"
msgstr ""

#: includes/common-functions.php:85
msgid ""
"The text will show above the packages list in user dashboard in my resume."
msgstr ""

#. Description of the plugin
msgid ""
"This addon is useful for exporting CVs like PDFs and excel 'xlsx' formate."
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:354
msgid "This package is useful for candidates to have multiple resume designs."
msgstr ""

#: templates/jobsearch-candidate-resume-template-five.php:189
#: templates/jobsearch-candidate-resume-template-five.php:378
msgid "Twitter"
msgstr ""

#: templates/jobsearch-candidate-resume-template-two.php:161
#: templates/jobsearch-candidate-resume-template-two.php:346
msgid "URL. "
msgstr ""

#: includes/user-uploaded-resume-export.php:23
#: includes/jobsearch-resume-export-admin-hooks.php:1022
msgid "User Uploaded Resumes"
msgstr ""

#: templates/jobsearch-candidate-resume-template-three.php:607
#: templates/jobsearch-candidate-resume-default-template.php:570
#: templates/jobsearch-candidate-resume-template-one.php:437
#: templates/jobsearch-candidate-resume-template-four.php:662
#: templates/jobsearch-candidate-resume-template-five.php:762
#: templates/jobsearch-candidate-resume-template-two.php:687
msgid "Video URL: "
msgstr ""

#: templates/jobsearch-candidate-resume-template-four.php:191
#: templates/jobsearch-candidate-resume-template-four.php:321
msgid "Website:"
msgstr ""

#: includes/common-functions.php:572
msgid "WooCommerce Plugin not exist."
msgstr ""

#: includes/jobsearch-resume-export-admin-hooks.php:137
#: includes/jobsearch-resume-export-admin-hooks.php:879
#: templates/jobsearch-candidate-resume-default-template.php:344
#: templates/jobsearch-candidate-resume-template-one.php:373
msgid "Work Experience"
msgstr ""

#: includes/common-functions.php:600
msgid "You are not a candidate."
msgstr ""

#: includes/common-functions.php:566
msgid "You have already subscribed to this package."
msgstr ""
