<?php

require_once "../../../wp-includes/http.php";

import_action();

function import_action()
{
    global $wpdb;

    $serviceUrl = 'http://localhost:8080/fetchAllJobs';
    $response = wp_remote_get($serviceUrl);

    $decodedJSON = json_decode(wp_remote_retrieve_body($response));


    $elementProcentValue = 100/sizeof($decodedJSON);
    $loopCounter = 0;

    foreach ($decodedJSON as $key => $job) {
        $posttitle = $job->employer->company;

        $employerID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "' and post_status ='publish'");

        if ($employerID == null){
            $employerID = createNewEmployer($job->employer);
        }

        $my_post = array(
            'post_title' => $job->post_title,
            'post_content' => $job->meta_input->content,
            'post_status' => 'publish',
            'post_author' => 1,
            'post_type' => 'job',
            'post_category' => array(8, 39),
            'meta_input' => array(
                'jobsearch_field_job_publish_date' => $job->meta_input->jobsearch_field_job_publish_date,
                'jobsearch_field_job_expiry_date' => $job->meta_input->jobsearch_field_job_expiry_date,
                //'job_actexpiry_date'	                        =>  '1606491999',
                'job_actexpiry_date' => '2606491999',
                'jobsearch_field_job_application_deadline_date' => $job->meta_input->jobsearch_field_job_application_deadline_date,
                'jobsearch_field_job_apply_type' => 'with_email',
                'jobsearch_field_job_apply_url' => $job->meta_input->jobsearch_field_job_apply_url,
                'jobsearch_field_job_apply_email' => 'papadakis@s-p-i-solutions.de',
                'jobsearch_field_job_salary' => $job->meta_input->jobsearch_field_job_salary,
                'jobsearch_field_job_max_salary' => $job->meta_input->jobsearch_field_job_max_salary,
                'jobsearch_field_job_salary_type' => 'type_1',
                'jobsearch_field_job_salary_currency' => 'default',
                'jobsearch_field_job_salary_pos' => 'left',
                'jobsearch_field_job_salary_sep' => ',',
                'jobsearch_field_job_salary_deci' => '2',
                'jobsearch_field_job_featured' => 'off',
                'jobsearch_field_job_feature_till' => '',
                'jobsearch_field_job_filled' => 'off',
                'jobsearch_field_job_posted_by' => $employerID,
                'jobsearch_field_job_status' => 'approved',
                'jobsearch_job_presnt_status' => 'approved',
                'jobsearch_field_location_location2' => $job->meta_input->jobsearch_field_location_location2,
                'jobsearch_field_location_address' => '',
                'jobsearch_location_old_postalcode' => '',
                'jobsearch_field_location_postalcode' => '',
                'jobsearch_field_location_lat' => '37.090240',
                'jobsearch_field_location_lng' => '-95.712891',
                'jobsearch_field_location_zoom' => '12',
                'jobsearch_field_map_height' => '250',
                'jobsearch_field_marker_image' => '',
                'jobsearch_field_package_exfield_title' => '',
                'jobsearch_field_package_exfield_val' => '',
                'jobsearch_job_single_exp_cron	yes' => '',
                'jobsearch_field_job_employer_count_updated' => 'yes',
                'jobsearch_job_employer_status' => 'approved',
                #'jobsearch_field_job_attachment_files' => 'a:0:{}',
                'jobsearch_job_views_count' => '1',
                'jobsearch_field_urgent_job' => 'off',
                'Industry' => '',
                '_urgent_job_frmadmin' => 'yes',
                'career-level' => 'manager',
                'careerfy_field_job_post_detail_style' => '',
                'content' => 'TestDescription',
                'cusjob_urgent_fbckend' => 'on',
                'experience' => '2-years',
                'gender' => 'male'
            ),

            'tax_input' => array(
                'jobtype' => array(
                    $job->tax_input[0]->jobtype, 'freelance', '137', '5'
                ))
        );

        $postID = wp_insert_post($my_post);

        echo ".";
     }

//     $arrayJSON = json_decode( $curl_response, true);

//     // Insert the post into the database    
//     //wp_insert_post( $my_post );
//     //echo "Success!!".$curl_response;
    echo "Success!!";
}


function createNewEmployer($employer){
    $employer_post = array(
        'post_title' => $employer->company,
        'post_content' => '',
        'post_status' => 'publish',
        'post_author' => 1,
        'post_type' => 'employer'
    );
    $employerID = wp_insert_post($employer_post);

    $upload_file = wp_upload_bits($employer->company_logo, null, file_get_contents($employer->company_logo));

    $wp_filetype = wp_check_filetype($employer->company_logo, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_parent' => $employerID,
        'post_title' => preg_replace('/\.[^.]+$/', '', $upload_file['file']),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    //wp_insert_attachment( $attachment, $filename, $parent_post_id );
    $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $employerID );

    $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
    wp_update_attachment_metadata( $attachment_id,  $attachment_data );
    set_post_thumbnail( $employerID, $attachment_id );

    return $employerID;
}

