<?php


namespace Bloatless\WebSocket\Application;


use Bloatless\WebSocket\Connection;

class JobImporterApplication extends Application
{
    public static $instance;

    private $clients = [];

    public function __construct() {
        self::$instance = $this;
    }

    public static function get() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function onConnect(Connection $client): void
    {
        echo "New Connection ".$client->getClientPort();
        $id = $client->getClientId();
        $this->clients[$id] = $client;
    }

    public function onDisconnect(Connection $client): void
    {
        $id = $client->getClientId();
        unset($this->clients[$id]);
    }

    public function onData(string $data, Connection $client): void
    {
        echo "Am senden   \n";
        $this->sendAll($data);

    }

    private function sendAll(string $encodedData): void
    {
        echo "Gesendet\n";
        if (count($this->clients) < 1) {
            return;
        }
        foreach ($this->clients as $sendto) {
            echo "An ".$sendto->getClientId()."\n";
            $sendto->send($encodedData);
        }
    }
}