<?php
/**
* The base configuration for WordPress
*
* The wp-config.php creation script uses this file during the
* installation. You don't have to use the web site, you can
* copy this file to "wp-config.php" and fill in the values.
*
* This file contains the following configurations:
*
* * MySQL settings
* * Secret keys
* * Database table prefix
* * ABSPATH
*
* @link https://wordpress.org/support/article/editing-wp-config-php/
*
* @package WordPress
*/



// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress before DWF*/  
define('DB_NAME', 'dwf');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1:3307');





/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY', 'GdEX/pk7O/Js2Od1HOBpzbOAAT9wdiFLzMpc+3hTLxGzIeroa2XAcbjfzuLUadtH');
define('SECURE_AUTH_KEY', 'WwaeAxAI86paSwu/+6BEyeumws9cif9wt9iG1fqCX0/mnUz5GYxmuRwxPyzRmOoo');
define('LOGGED_IN_KEY', 'l7LU9TTbwpyzJmsmN4SEe4bxmm7C8MAyYjrWlO1vfJHWhIa7y2b2BCDGmWZ5ZRu1');
define('NONCE_KEY', 'OhhDzRcGU1DDWeKTTLim216ASN4eejl0zIzvElikXd9QcD44xciZaXvmvPgB/Mmf');
define('AUTH_SALT', '5qmyXd6QIMahAqa/Q1p6HOPKfKHSo+uudO8LnCI6jbD7EnogXqepLzyKrF5rg3tf');
define('SECURE_AUTH_SALT', '6wRtUfRuHk9caDrO98kRkEFipJOwbexuOAfXTS65fj8LXwSG17ERyWYNDdAUI7hT');
define('LOGGED_IN_SALT', 'kRANLIXF9/zjFYeXMbWpceo5sOlblWe9qFLdS+0eiBeMF/EvVTvbs7a6AvGm/jpa');
define('NONCE_SALT', 'RBLv522zEp+FcYCi92smGX382HMhnmjVwYnAhK11CARSmFE+kab/9i9WNtRf8nLy');

/**#@-*/

/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each
* a unique prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = 'dgd_';

/**
* For developers: WordPress debugging mode.
*
* Change this to true to enable the display of notices during development.
* It is strongly recommended that plugin and theme developers use WP_DEBUG
* in their development environments.
*
* For information on other constants that can be used for debugging,
* visit the documentation.
*
* @link https://wordpress.org/support/article/debugging-in-wordpress/
*/
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

define('SAVEQUERIES', true);

