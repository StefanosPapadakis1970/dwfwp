
function openImporter(){
	alert("Ajax URL"+ajaxurl);
$.ajax({
    url: ajaxurl, // this is the object instantiated in wp_localize_script function
    type: 'GET',
    data:{ 
      action: 'importJobsAction', // this is the function in your functions.php that will be triggered
      name: 'John',
      age: '38'
    },
    success: function( data ){
      $("#wpbody-content").html(data);
      console.log( data );
    }
  });
}

function importAction(){
	alert("ImportAction");
    $("#progress").html("Retrieving Records from WebService");
	var data = {
			'action': 'import_action',
			'whatever': ajax_object.we_value      // We pass php values differently!
		};
		// We can also pass the url value separately from ajaxurl for front end AJAX implementations
		jQuery.post(ajax_object.ajax_url, data, function(response) {
			//alert('Got this from the server: ' + response);
		});	
}


function loadtestAction(){
    alert("LoadtestAction");
    var data = {
        'action': 'loadtest_action',
        'whatever': ajax_object.we_value      // We pass php values differently!
    };
    // We can also pass the url value separately from ajaxurl for front end AJAX implementations
    jQuery.post(ajax_object.ajax_url, data, function(response) {
        alert('Got this from the server: ' + response);
    });
}

var counter = 0;
function progress(){
    counter++;
    $("#progress").html(counter);
}


// Connect to websocket server


///////////// Websocket Server connection
var socket = null;
var springWebSocket = null;
var webSocketServerUrl = 'ws://localhost:8000/dwf/jobimporter';
var springWebSocketUrl = 'ws://localhost:8080/dataFetchStatus';

function setUpWebSocket() {
    alert("SetUpSocketServer");
    if (window.MozWebSocket) {
        socket = new MozWebSocket(webSocketServerUrl);
        springWebSocket = new MozWebSocket(springWebSocketUrl);
    } else if (window.WebSocket) {
        socket = new WebSocket(webSocketServerUrl);
        springWebSocket = new WebSocket(springWebSocketUrl);
    }

    socket.onerror=function(event){
        alert("Could not Connect To Status Server !"+event);
        $("#startButton").fadeOut();
        console.log("Error");
    }
    /**
     * Called when connected to websocket server.
     * @param {Object} msg
     */
    socket.onopen = (msg) => {
        alert("Opening JavaScriptSocket");
    };

    /**
     * Called when disconnected from websocket server.
     * @param {Object} msg
     */
    socket.onclose = (msg) => {
    };

    /**
     * Called when a message is received from websocket server.
     * @param {Object} msg
     * @returns {*}
     */
    socket.onmessage = (msg) => {
        let response = JSON.parse(msg.data);
        switch (response.action) {
            case "initProgressBar":
                return "Status";
            case "setProgress":
                $("#orangeProgressBar").attr('style','width: '+response.data);
                $("#progress").html(response.filename);
                return "";
            case "clientDisconnected":
                return "Disconnected";
            case "clientActivity":
                return "Client Activity";
            case "serverInfo":
                return "ServerInfo";
        }
    };



    springWebSocket.onerror=function(event){
        alert("Could not Connect To Spring Server !"+event);
        $("#startButton").fadeOut();
        console.log("Error");
    }
    /**
     * Called when connected to websocket server.
     * @param {Object} msg
     */
    springWebSocket.onopen = (msg) => {
        alert("Opening SpringWebSocket");
    };

    /**
     * Called when disconnected from websocket server.
     * @param {Object} msg
     */
    springWebSocket.onclose = (msg) => {
    };

    /**
     * Called when a message is received from websocket server.
     * @param {Object} msg
     * @returns {*}
     */

    var totalNumberOfRecords = 0;
    var incrementBy = 0;
    var currentProgress = 0;

    springWebSocket.onmessage = (msg) => {
        console.log(msg.data);
        if (msg.data == 'progress'){
            currentProgress = currentProgress + incrementBy;
            console.log(currentProgress+" "+incrementBy);
            $("#blueProgressBar").attr('style','width: '+currentProgress+'%');
        } else {
            let response = JSON.parse(msg.data);
            totalNumberOfRecords = response.totalNumberOfRecords;
            incrementBy = 100 /totalNumberOfRecords ;
            //alert(totalNumberOfRecords+ ""+incrementBy);
        }
    };


}